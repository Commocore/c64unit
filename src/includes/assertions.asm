
; @access public
; @param X - lo-byte address of Data Set
; @param Y - hi-byte address of Data Set
; @uses c64unit_dataSetIterator
; @return void
c64unit_AssertDataSetEqual
	stx c64unit_AssertDataSetEqualPointer+1
	sty c64unit_AssertDataSetEqualPointer+2
	
	ldx #<c64unit_FailMessage.assertEqual
	ldy #>c64unit_FailMessage.assertEqual
	lda #size(c64unit_FailMessage.assertEqual)
	jsr c64unit_PrepareAssertMessage
	
	ldy c64unit_dataSetIterator
c64unit_AssertDataSetEqualPointer
	lda $1234,y
	sta c64unit_expected
	cmp c64unit_actual
	beq +
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @param X - lo-byte address of Data Set
; @param Y - hi-byte address of Data Set
; @uses c64unit_dataSetIterator
; @return void
c64unit_AssertDataSetWordEqual
	stx c64unit_AssertDataSetWordEqualPointer+1
	sty c64unit_AssertDataSetWordEqualPointer+2
	
	ldx #<c64unit_FailMessage.assertEqual
	ldy #>c64unit_FailMessage.assertEqual
	lda #size(c64unit_FailMessage.assertEqual)
	jsr c64unit_PrepareAssertMessage
	
	lda c64unit_dataSetIterator
	asl
	tay
c64unit_AssertDataSetWordEqualPointer
	lda $1234,y
	sta c64unit_expected
	cmp c64unit_actual
	beq +
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @return void
c64unit_AssertEqual
	ldx #<c64unit_FailMessage.assertEqual
	ldy #>c64unit_FailMessage.assertEqual
	lda #size(c64unit_FailMessage.assertEqual)
	jsr c64unit_PrepareAssertMessage
	
	lda c64unit_expected
	cmp c64unit_actual
	beq +
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @return void
c64unit_AssertFlagSet
	lda c64unit_actual
	bne +
		lda #1
		sta c64unit_expected
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @return void
c64unit_AssertGreater
	ldx #<c64unit_FailMessage.assertGreater
	ldy #>c64unit_FailMessage.assertGreater
	lda #size(c64unit_FailMessage.assertGreater)
	jsr c64unit_PrepareAssertMessage
	
	lda c64unit_expected
	cmp c64unit_actual
	beq +
		bcs ++
+
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @return void
c64unit_AssertGreaterOrEqual
	ldx #<c64unit_FailMessage.assertGreaterOrEqual
	ldy #>c64unit_FailMessage.assertGreaterOrEqual
	lda #size(c64unit_FailMessage.assertGreaterOrEqual)
	jsr c64unit_PrepareAssertMessage
	
	lda c64unit_expected
	cmp c64unit_actual
	bcs +
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @return void
c64unit_AssertLess
	ldx #<c64unit_FailMessage.assertLess
	ldy #>c64unit_FailMessage.assertLess
	lda #size(c64unit_FailMessage.assertLess)
	jsr c64unit_PrepareAssertMessage
	
	lda c64unit_expected
	cmp c64unit_actual
	bcc +
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @param X - length lo-byte value
; @param Y - length hi-byte value
; @return void
c64unit_AssertMemoryEqual
	stx c64unit_MemoryLocationLengthLo+1
	sty c64unit_MemoryLocationLengthHi+1
	
	lda #0
	sta c64unit_numberLo
	sta c64unit_numberHi
-
c64unit_ExpectedMemoryLocationPointer
	lda $1234
c64unit_ActualMemoryLocationPointer
	cmp $1234
	beq +
		ldx #<c64unit_FailMessage.assertMemoryEqual
		ldy #>c64unit_FailMessage.assertMemoryEqual
		lda #size(c64unit_FailMessage.assertMemoryEqual)
		jsr c64unit_PrepareAssertMessage
	
		ldx c64unit_ExpectedMemoryLocationPointer+1
		ldy c64unit_ExpectedMemoryLocationPointer+2
		jsr c64unit_SetExpectedAbsoluteByte
		
		ldx c64unit_ActualMemoryLocationPointer+1
		ldy c64unit_ActualMemoryLocationPointer+2
		jsr c64unit_SetActualAbsoluteByte
		
		lda #1
		sta c64unit_DataSetLength+1
		
		lda c64unit_numberLo
		sta c64unit_dataSetIterator
		lda c64unit_numberHi
		sta c64unit_dataSetIterator+1
		
		jmp c64unit_AssertionFailed
+
	
	clc
	lda c64unit_numberLo
	adc #1
	sta c64unit_numberLo
	lda c64unit_numberHi
	adc #0
	sta c64unit_numberHi

c64unit_MemoryLocationLengthHi
	cmp #0
	bne +
		lda c64unit_numberLo
c64unit_MemoryLocationLengthLo
		cmp #0
		bne +
			jsr c64unit_PrintProgressIndicator
			rts
+
	clc
	lda c64unit_ExpectedMemoryLocationPointer+1
	adc #1
	sta c64unit_ExpectedMemoryLocationPointer+1
	lda c64unit_ExpectedMemoryLocationPointer+2
	adc #0
	sta c64unit_ExpectedMemoryLocationPointer+2
	
	clc
	lda c64unit_ActualMemoryLocationPointer+1
	adc #1
	sta c64unit_ActualMemoryLocationPointer+1
	lda c64unit_ActualMemoryLocationPointer+2
	adc #0
	sta c64unit_ActualMemoryLocationPointer+2
jmp -


; @access public
; @return void
c64unit_AssertNotEqual
	ldx #<c64unit_FailMessage.assertNotEqual
	ldy #>c64unit_FailMessage.assertNotEqual
	lda #size(c64unit_FailMessage.assertNotEqual)
	jsr c64unit_PrepareAssertMessage
	
	lda c64unit_expected
	cmp c64unit_actual
	bne +
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @return void
c64unit_AssertWordEqual
	ldx #<c64unit_FailMessage.assertWordEqual
	ldy #>c64unit_FailMessage.assertWordEqual
	lda #size(c64unit_FailMessage.assertWordEqual)
	jsr c64unit_PrepareAssertMessage
	
	lda c64unit_expected
	cmp c64unit_actual
	beq +
		jmp c64unit_AssertionFailed
	+
	lda c64unit_expected+1
	cmp c64unit_actual+1
	beq +
		jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @return void
c64unit_AssertWordNotEqual
	ldx #<c64unit_FailMessage.assertWordNotEqual
	ldy #>c64unit_FailMessage.assertWordNotEqual
	lda #size(c64unit_FailMessage.assertWordNotEqual)
	jsr c64unit_PrepareAssertMessage
	
	lda c64unit_expected
	cmp c64unit_actual
	bne +
		lda c64unit_expected+1
		cmp c64unit_actual+1
		bne +
			jmp c64unit_AssertionFailed
+
	jsr c64unit_PrintProgressIndicator
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreCarryFlagStatus
	bcc +
		lda #1
		.byte $2c
+
	lda #0
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertCarryFlag
	ldy #>c64unit_FailMessage.assertCarryFlag
	lda #size(c64unit_FailMessage.assertCarryFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreDecimalFlagStatus
	clc
	lda #$5
	adc #$5
	cmp #$10
	bne +
		lda #1
		.byte $2c
+
	lda #0
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertDecimalFlag
	ldy #>c64unit_FailMessage.assertDecimalFlag
	lda #size(c64unit_FailMessage.assertDecimalFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreNegativeFlagStatusSigned
	bpl +
		lda #1
		.byte $2c
+
	lda #0
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertNegativeFlag
	ldy #>c64unit_FailMessage.assertNegativeFlag
	lda #size(c64unit_FailMessage.assertNegativeFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreNegativeFlagStatusUnsigned
	bcs +
		lda #1
		.byte $2c
+
	lda #0
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertNegativeFlag
	ldy #>c64unit_FailMessage.assertNegativeFlag
	lda #size(c64unit_FailMessage.assertNegativeFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreNotCarryFlagStatus
	bcc +
		lda #0
		.byte $2c
+
	lda #1
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertNotCarryFlag
	ldy #>c64unit_FailMessage.assertNotCarryFlag
	lda #size(c64unit_FailMessage.assertNotCarryFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreNotDecimalFlagStatus
	clc
	lda #$5
	adc #$5
	cmp #$10
	bne +
		lda #0
		.byte $2c
+
	lda #1
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertNotDecimalFlag
	ldy #>c64unit_FailMessage.assertNotDecimalFlag
	lda #size(c64unit_FailMessage.assertNotDecimalFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreNotNegativeFlagStatusSigned
	bpl +
		lda #0
		.byte $2c
+
	lda #1
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertNotNegativeFlag
	ldy #>c64unit_FailMessage.assertNotNegativeFlag
	lda #size(c64unit_FailMessage.assertNotNegativeFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreNotNegativeFlagStatusUnsigned
	bcs +
		lda #0
		.byte $2c
+
	lda #1
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertNotNegativeFlag
	ldy #>c64unit_FailMessage.assertNotNegativeFlag
	lda #size(c64unit_FailMessage.assertNotNegativeFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreNotOverflowFlagStatus
	bvc +
		lda #0
		.byte $2c
+
	lda #1
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertNotOverflowFlag
	ldy #>c64unit_FailMessage.assertNotOverflowFlag
	lda #size(c64unit_FailMessage.assertNotOverflowFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreNotZeroFlagStatus
	bne +
		lda #0
		.byte $2c
+
	lda #1
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertNotZeroFlag
	ldy #>c64unit_FailMessage.assertNotZeroFlag
	lda #size(c64unit_FailMessage.assertNotZeroFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreOverflowFlagStatus
	bvc +
		lda #1
		.byte $2c
+
	lda #0
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertOverflowFlag
	ldy #>c64unit_FailMessage.assertOverflowFlag
	lda #size(c64unit_FailMessage.assertOverflowFlag)
	jsr c64unit_PrepareAssertMessage
rts


; @access public
; @uses c64unit_actual
; @return void
c64unit_StoreZeroFlagStatus
	bne +
		lda #1
		.byte $2c
+
	lda #0
	sta c64unit_actual
	
	ldx #<c64unit_FailMessage.assertZeroFlag
	ldy #>c64unit_FailMessage.assertZeroFlag
	lda #size(c64unit_FailMessage.assertZeroFlag)
	jsr c64unit_PrepareAssertMessage
rts


.enc "screen"

c64unit_FailMessage .proc
assertEqual .text "assert equal failed."
assertNotEqual .text "assert not equal failed."
assertCarryFlag .text "assert carry flag failed."
assertDecimalFlag .text "assert decimal flag failed."
assertNegativeFlag .text "assert negative flag failed."
assertOverflowFlag .text "assert overflow flag failed."
assertZeroFlag .text "assert zero flag failed."
assertNotCarryFlag .text "assert not carry flag failed."
assertNotDecimalFlag .text "assert not decimal flag failed."
assertNotNegativeFlag .text "assert not negative flag failed."
assertNotOverflowFlag .text "assert not overflow flag failed."
assertNotZeroFlag .text "assert not zero flag failed."
assertGreater .text "assert greater failed."
assertGreaterOrEqual .text "assert greater or equal failed."
assertLess .text "assert less failed."
assertWordEqual .text "assert word equal failed."
assertWordNotEqual .text "assert word not equal failed."
assertMemoryEqual .text "assert memory equal failed."
.pend

.enc "none"
