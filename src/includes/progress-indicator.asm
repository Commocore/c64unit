﻿
; @access private
; @return void
c64unit_CountIndicatorRows
	clc
c64unit_CountIndicatorRowsPointer
	lda #0
	adc #1
	sta c64unit_CountIndicatorRowsPointer+1
	cmp #40
	bne +
		inc c64unit_SetCursorRowOnExitToBasic+1
		lda #0
		sta c64unit_CountIndicatorRowsPointer+1
+
rts


; @access public
; @return void
c64unit_IncrementProgressIndicator
	lda c64unit_ProgressIndicatorPointer+1
	clc
	adc #1
	sta c64unit_ProgressIndicatorPointer+1
	lda c64unit_ProgressIndicatorPointer+2
	adc #0
	sta c64unit_ProgressIndicatorPointer+2
	
	jsr c64unit_IsExitToBasicMode
	beq +
		jsr c64unit_CountIndicatorRows
+
rts


; @access public
; @return void
c64unit_InitProgressIndicator
	sec
	lda c64unit_screenLocation
	sbc #1
	sta c64unit_ProgressIndicatorPointer+1
	lda c64unit_screenLocation+1
	sbc #0
	sta c64unit_ProgressIndicatorPointer+2

	jsr c64unit_IsExitToBasicMode
	beq +
		lda #0
		sta c64unit_CountIndicatorRowsPointer+1
		sta c64unit_SetCursorRowOnExitToBasic+1
+
rts


; Show testsuite progress by putting indicator on the screen
;
; @access public
; @return void
c64unit_PrintProgressIndicator
	cld
	jsr c64unit_IncrementProgressIndicator
	lda #46 ; dot character
c64unit_ProgressIndicatorPointer
	sta $1234
rts
