<?php

namespace Commocore\C64Unit\Symlinker\Symbols\Strategy;

use Commocore\C64Unit\Symlinker\IO\SymlinkReader;
use Commocore\C64Unit\Symlinker\Symbols\SymbolsList;

interface SymbolsInterface
{
    public function __construct(SymlinkReader $symlinkReader);

    /**
     * @return SymbolsList
     */
    public function getSymbolsList();

    /**
     * @return string
     */
    public function getOutputDirectory();


    /**
     * @return string
     */
    public function getComment();
}
