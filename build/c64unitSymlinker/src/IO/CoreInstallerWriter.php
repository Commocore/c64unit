<?php

namespace Commocore\C64Unit\Symlinker\IO;

use Commocore\C64Unit\Symlinker\Configuration;
use Commocore\C64Unit\Symlinker\CoreInstallers\Strategy\CoreInstallerInterface;

class CoreInstallerWriter
{
    /**
     * @var Configuration
     */
    private $configuration;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    public function save(CoreInstallerInterface $coreInstaller)
    {
        foreach ($this->configuration->getCorePages() as $page) {
            $filename = '../../cross-assemblers/' .
                $this->configuration->getOutputDirectory($coreInstaller->getName()) .
                '/core' . $page . '.asm';

            $fp = fopen($filename, "w") or die('Cannot open file to save');
            fputs($fp, $coreInstaller->getContent($page));
            fclose($fp);
            $this->displayMessage($filename);
        }
    }

    /**
     * @param string $filename
     */
    private function displayMessage($filename)
    {
        echo 'Core installer saved in ' . $filename . PHP_EOL;
    }
}
