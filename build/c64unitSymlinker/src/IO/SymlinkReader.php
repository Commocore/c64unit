<?php

namespace Commocore\C64Unit\Symlinker\IO;

class SymlinkReader
{
    /**
     * @var array
     */
    private $data = array();

    /**
     * @var SymlinkSourceFile
     */
    private $symlinkFile;

    /**
     * @param SymlinkSourceFile $symlinkFile
     */
    public function __construct(SymlinkSourceFile $symlinkFile)
    {
        $this->symlinkFile = $symlinkFile;
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (empty($this->data)) {
            $this->readData();
        }
        return $this->data;
    }

    private function readData()
    {
        while ($line = $this->symlinkFile->getLine()) {
            $explodedLine = explode('=', $line);
            $this->data[] = array(
                'key' => trim($explodedLine[0]),
                'address' => trim($explodedLine[1])
            );
        }
    }
}
