
; This file is automatically generated by Symlinker

coreBinaryLocation equ $3000

	if c64unit_include == "definitions"
		include "cross-assemblers/dasm/symbols.asm"
		include "cross-assemblers/dasm/macros.asm"
	else if c64unit_include == "package"
		org coreBinaryLocation
		incbin "bin/core3000.bin"
	eif
