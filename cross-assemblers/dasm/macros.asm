
	; @param word expected
	; @param word actual
	; @param string customMessage = ""
	mac assertAbsoluteWordEqual
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_SetExpectedAbsoluteWord
		
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_SetActualAbsoluteWord
		
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertWordEqual
	endm

	
	; @param word expected
	; @param word actual
	; @param string customMessage = ""
	mac assertAbsoluteWordNotEqual
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_SetExpectedAbsoluteWord
		
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_SetActualAbsoluteWord
		
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertWordNotEqual
	endm


	; @param string customMessage = ""
	mac assertCarryFlagNotSet
		jsr c64unit_StoreNotCarryFlagStatus
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm
	
	
	; @param string customMessage = ""
	mac assertCarryFlagSet
		jsr c64unit_StoreCarryFlagStatus
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm


	; @param word expectedDataSet
	; @param byte actual
	; @param string customMessage = ""
	mac assertDataSetEqual
		lda {2}
		sta c64unit_actual
		if {3} != ""
			prepareCustomMessage {3}
		eif
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_AssertDataSetEqual
	endm


	; @param word expectedDataSet
	; @param string customMessage = ""
	mac assertDataSetEqualToA
		sta c64unit_actual
		if {2} != ""
			prepareCustomMessage {2}
		eif
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_AssertDataSetEqual
	endm


	; @param word expectedDataSet
	; @param string customMessage = ""
	mac assertDataSetEqualToX
		stx c64unit_actual
		if {2} != ""
			prepareCustomMessage {2}
		eif
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_AssertDataSetEqual
	endm


	; @param word expectedDataSet
	; @param string customMessage = ""
	mac assertDataSetEqualToY
		sty c64unit_actual
		if {2} != ""
			prepareCustomMessage {2}
		eif
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_AssertDataSetEqual
	endm

	
	; @param word expectedDataSet
	; @param word actual
	; @param string customMessage = ""
	mac assertDataSetWordEqual
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_SetExpectedAbsoluteWord
		
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_SetActualAbsoluteWord
		
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertWordEqual
	endm

	
	; @param string customMessage = ""
	mac assertDecimalFlagNotSet
		jsr c64unit_StoreNotDecimalFlagStatus
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm
	
	
	; @param string customMessage = ""
	mac assertDecimalFlagSet
		jsr c64unit_StoreDecimalFlagStatus
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm


	; @param byte expected
	; @param byte actual
	; @param string customMessage = ""
	mac assertEqual
		lda {1}
		sta c64unit_expected
		lda {2}
		sta c64unit_actual
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertEqual
	endm


	; @param byte expected
	; @param string customMessage = ""
	mac assertEqualToA
		sta c64unit_actual
		lda {1}
		sta c64unit_expected
		if {2} != ""
			prepareCustomMessage {2}
		eif
		jsr c64unit_AssertEqual
	endm


	; @param byte expected
	; @param string customMessage = ""
	mac assertEqualToX
		stx c64unit_actual
		lda {1}
		sta c64unit_expected
		if {2} != ""
			prepareCustomMessage {2}
		eif
		jsr c64unit_AssertEqual
	endm


	; @param byte expected
	; @param string customMessage = ""
	mac assertEqualToY
		sty c64unit_actual
		lda {1}
		sta c64unit_expected
		if {2} != ""
			prepareCustomMessage {2}
		eif
		jsr c64unit_AssertEqual
	endm


	; @param byte expected
	; @param byte actual
	; @param string customMessage = ""
	mac assertGreater
		lda {1}
		sta c64unit_expected
		lda {2}
		sta c64unit_actual
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertGreater
	endm


	; @param byte expected
	; @param byte actual
	; @param string customMessage = ""
	mac assertGreaterOrEqual
		lda {1}
		sta c64unit_expected
		lda {2}
		sta c64unit_actual
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertGreaterOrEqual
	endm


	; @param byte expected
	; @param byte actual
	; @param string customMessage = ""
	mac assertLess
		lda {1}
		sta c64unit_expected
		lda {2}
		sta c64unit_actual
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertLess
	endm

	
	; @param word expected
	; @param word actual
	; @param word length
	; @param string customMessage = ""
	mac assertMemoryEqual
		lda #<{1}
		sta c64unit_ExpectedMemoryLocationPointer+1
		lda #>{1}
		sta c64unit_ExpectedMemoryLocationPointer+2
		
		lda #<{2}
		sta c64unit_ActualMemoryLocationPointer+1
		lda #>{2}
		sta c64unit_ActualMemoryLocationPointer+2
		
		if {4} != ""
			prepareCustomMessage {4}
		eif
		
		ldx #<{3}
		ldy #>{3}
		jsr c64unit_AssertMemoryEqual
	.endm


	; @param string customMessage = ""
	mac assertNegativeFlagSignedNotSet
		jsr c64unit_StoreNotNegativeFlagStatusSigned
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm
	
	
	; @param string customMessage = ""
	mac assertNegativeFlagSignedSet
		jsr c64unit_StoreNegativeFlagStatusSigned
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm

	
	; @param string customMessage = ""
	mac assertNegativeFlagUnsignedNotSet
		jsr c64unit_StoreNotNegativeFlagStatusUnsigned
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm
	
	
	; @param string customMessage = ""
	mac assertNegativeFlagUnsignedSet
		jsr c64unit_StoreNegativeFlagStatusUnsigned
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm
	

	; @param byte expected
	; @param byte actual
	; @param string customMessage = ""
	mac assertNotEqual
		lda {1}
		sta c64unit_expected
		lda {2}
		sta c64unit_actual
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertNotEqual
	endm


	; @param byte expected
	; @param string customMessage = ""
	mac assertNotEqualToA
		sta c64unit_actual
		lda {1}
		sta c64unit_expected
		if {2} != ""
			prepareCustomMessage {2}
		eif
		jsr c64unit_AssertNotEqual
	endm


	; @param byte expected
	; @param string customMessage = ""
	mac assertNotEqualToX
		stx c64unit_actual
		lda {1}
		sta c64unit_expected
		if {2} != ""
			prepareCustomMessage {2}
		eif
		jsr c64unit_AssertNotEqual
	endm


	; @param byte expected
	; @param string customMessage = ""
	mac assertNotEqualToY
		sty c64unit_actual
		lda {1}
		sta c64unit_expected
		if {2} != ""
			prepareCustomMessage {2}
		eif
		jsr c64unit_AssertNotEqual
	endm
	
	
	; @param string customMessage = ""
	mac assertOverflowFlagNotSet
		jsr c64unit_StoreNotOverflowFlagStatus
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm
	
	
	; @param string customMessage = ""
	mac assertOverflowFlagSet
		jsr c64unit_StoreOverflowFlagStatus
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm


	; @param word expected
	; @param word actual
	; @param string customMessage = ""
	mac assertWordEqual
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_SetExpectedImmediateWord
		
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_SetActualAbsoluteWord
		
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertWordEqual
	endm

	
	; @param word expected
	; @param word actual
	; @param string customMessage = ""
	mac assertWordNotEqual
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_SetExpectedImmediateWord
		
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_SetActualAbsoluteWord
		
		if {3} != ""
			prepareCustomMessage {3}
		eif
		jsr c64unit_AssertWordNotEqual
	endm
	

	; @param string customMessage = ""
	mac assertZeroFlagNotSet
		jsr c64unit_StoreNotZeroFlagStatus
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm
	
	
	; @param string customMessage = ""
	mac assertZeroFlagSet
		jsr c64unit_StoreZeroFlagStatus
		if {1} != ""
			prepareCustomMessage {1}
		eif
		jsr c64unit_AssertFlagSet
	endm


	; @param byte exitToBasicMode
	; @param word screenLocation
	mac c64unit

		org $801

		dc.w nextBasicLine
		dc.w 10
		hex 9e
		if start
			dc.b [start]d
		eif
		hex 00
nextBasicLine
		hex 00 00

start
		lda #{1}
		jsr c64unit_InitExitToBasicMode
		
		if {2} == 0
			ldx #<$400
			ldy #>$400
		else
			ldx #<{2}
			ldy #>{2}
		eif
		jsr c64unit_InitScreenLocation
		
		jsr c64unit_InitScreen
	endm


	mac c64unitExit
		jmp c64unit_ExitWithSuccess
	endm


	; @param word testMethod
	mac examineTest
		jsr c64unit_InitTest
		jsr {1}
	endm


	; @param word dataSet
	; @param word output
	mac loadDataSet
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_PrepareDataSetOutput
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_LoadDataSet
	endm
	
	
	; @param word dataSet
	mac loadDataSetToA
		pushXYOnStack
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_LoadDataSetToA
		sta c64unit_number
		pullXYFromStack
		lda c64unit_number
	endm

	
	; @param word dataSet
	; @param word output
	mac loadDataSetWord
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_PrepareDataSetWordOutput
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_LoadDataSetWord
	endm
	
	
	; @param word dataSet
	; @param word outputLo
	; @param word outputHi
	mac loadDataSetWordToLoHi
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_PrepareDataSetWordLoOutput
		ldx #<{3}
		ldy #>{3}
		jsr c64unit_PrepareDataSetWordHiOutput
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_LoadDataSetWordToLoHi
	endm


	mac initTest
		jsr c64unit_InitTest
	endm


	; @return carry flag
	mac isDataSetCompleted
		jsr c64unit_IsDataSetCompleted
	endm

	
	; @param word originalMethod
	; @param word mockMethod
	mac mockMethod
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_SetOriginalMethodPointer
		
		ldx #<{2}
		ldy #>{2}
		jsr c64unit_SetMockMethodPointer
		
		jsr c64unit_MockMethod
	endm


	; @param string customMessage
	mac prepareCustomMessage
		; Trick to create text label dynamically
		jmp .1
.text
	.byte {1}
.textEnd

.1
		; Calculate length of a message
		sec
		lda #<.textEnd
		sbc #<.text
		
		; Convert message characters
		ldx #<.text
		ldy #>.text
		jsr c64unit_PrepareCustomMessageWithConversion
	endm


	; @param byte length
	mac prepareDataSetLength
		lda #{1}
		jsr c64unit_PrepareDataSetLength
	endm


	mac pushXYOnStack
		txa
		pha
		tya
		pha
	endm


	mac pullXYFromStack
		pla
		tay
		pla
		tax
	endm

	
	; @param word originalMethod
	mac unmockMethod
		ldx #<{1}
		ldy #>{1}
		jsr c64unit_UnmockMethod
	endm
