
; @param word .expected
; @param word .actual
!macro assertAbsoluteWordEqual .expected, .actual {
	ldx #<.expected
	ldy #>.expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	jsr c64unit_AssertWordEqual
}


; @param word .expected
; @param word .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertAbsoluteWordEqual .expected, .actual, .customMessage, .customMessageEnd {
	ldx #<.expected
	ldy #>.expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertWordEqual
}


; @param word .expected
; @param word .actual
!macro assertAbsoluteWordNotEqual .expected, .actual {
	ldx #<.expected
	ldy #>.expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	jsr c64unit_AssertWordNotEqual
}


; @param word .expected
; @param word .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertAbsoluteWordNotEqual .expected, .actual, .customMessage, .customMessageEnd {
	ldx #<.expected
	ldy #>.expected
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertWordNotEqual
}


!macro assertCarryFlagNotSet {
	jsr c64unit_StoreNotCarryFlagStatus
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertCarryFlagNotSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreNotCarryFlagStatus
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


!macro assertCarryFlagSet {
	jsr c64unit_StoreCarryFlagStatus
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertCarryFlagSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreCarryFlagStatus
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


; @param word .expectedDataSet
; @param byte .actual
!macro assertDataSetEqual .expectedDataSet, .actual {
	lda .actual
	sta c64unit_actual
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


; @param word .expectedDataSet
; @param byte .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertDataSetEqual .expectedDataSet, .actual, .customMessage, .customMessageEnd {
	lda .actual
	sta c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


; @param word .expectedDataSet
!macro assertDataSetEqualToA .expectedDataSet {
	sta c64unit_actual
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


; @param word .expectedDataSet
; @param word .customMessage
; @param word .customMessageEnd
!macro assertDataSetEqualToA .expectedDataSet, .customMessage, .customMessageEnd {
	sta c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


; @param word .expectedDataSet
!macro assertDataSetEqualToX .expectedDataSet {
	stx c64unit_actual
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


; @param word .expectedDataSet
; @param word .customMessage
; @param word .customMessageEnd
!macro assertDataSetEqualToX .expectedDataSet, .customMessage, .customMessageEnd {
	stx c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


; @param word .expectedDataSet
!macro assertDataSetEqualToY .expectedDataSet {
	sty c64unit_actual
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


; @param word .expectedDataSet
; @param word .customMessage
; @param word .customMessageEnd
!macro assertDataSetEqualToY .expectedDataSet, .customMessage, .customMessageEnd {
	sty c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_AssertDataSetEqual
}


; @param word .expectedDataSet
; @param word .actual
!macro assertDataSetWordEqual .expectedDataSet, .actual {
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	jsr c64unit_AssertWordEqual
}


; @param word .expectedDataSet
; @param word .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertDataSetWordEqual .expectedDataSet, .actual, .customMessage, .customMessageEnd {
	ldx #<.expectedDataSet
	ldy #>.expectedDataSet
	jsr c64unit_SetExpectedAbsoluteWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertWordEqual
}


!macro assertDecimalFlagNotSet {
	jsr c64unit_StoreNotDecimalFlagStatus
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertDecimalFlagNotSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreNotDecimalFlagStatus
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


!macro assertDecimalFlagSet {
	jsr c64unit_StoreDecimalFlagStatus
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertDecimalFlagSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreDecimalFlagStatus
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


; @param byte .expected
; @param byte .actual
!macro assertEqual .expected, .actual {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	jsr c64unit_AssertEqual
}


; @param byte .expected
; @param byte .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertEqual .expected, .actual, .customMessage, .customMessageEnd {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertEqual
}


; @param byte .expected
!macro assertEqualToA .expected {
	sta c64unit_actual
	lda #.expected
	sta c64unit_expected
	jsr c64unit_AssertEqual
}


; @param byte .expected
; @param word .customMessage
; @param word .customMessageEnd
; @param word .customMessageEnd
!macro assertEqualToA .expected, .customMessage, .customMessageEnd {
	sta c64unit_actual
	lda #.expected
	sta c64unit_expected
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertEqual
}


; @param byte .expected
!macro assertEqualToX .expected {
	stx c64unit_actual
	lda #.expected
	sta c64unit_expected
	jsr c64unit_AssertEqual
}


; @param byte .expected
; @param word .customMessage
; @param word .customMessageEnd
!macro assertEqualToX .expected, .customMessage, .customMessageEnd {
	stx c64unit_actual
	lda #.expected
	sta c64unit_expected
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertEqual
}


; @param byte .expected
!macro assertEqualToY .expected {
	sty c64unit_actual
	lda #.expected
	sta c64unit_expected
	jsr c64unit_AssertEqual
}


; @param byte .expected
; @param word .customMessage
; @param word .customMessageEnd
!macro assertEqualToY .expected, .customMessage, .customMessageEnd {
	sty c64unit_actual
	lda #.expected
	sta c64unit_expected
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertEqual
}


; @param byte .expected
; @param byte .actual
!macro assertGreater .expected, .actual {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	jsr c64unit_AssertGreater
}


; @param byte .expected
; @param byte .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertGreater .expected, .actual, .customMessage, .customMessageEnd {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertGreater
}


; @param byte .expected
; @param byte .actual
!macro assertGreaterOrEqual .expected, .actual {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	jsr c64unit_AssertGreaterOrEqual
}


; @param byte .expected
; @param byte .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertGreaterOrEqual .expected, .actual, .customMessage, .customMessageEnd {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertGreaterOrEqual
}


; @param byte .expected
; @param byte .actual
!macro assertLess .expected, .actual {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	jsr c64unit_AssertLess
}


; @param byte .expected
; @param byte .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertLess .expected, .actual, .customMessage, .customMessageEnd {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertLess
}


; @param word .expected
; @param word .actual
; @param word .length
!macro assertMemoryEqual .expected, .actual, .length {
	lda #<.expected
	sta c64unit_ExpectedMemoryLocationPointer+1
	lda #>.expected
	sta c64unit_ExpectedMemoryLocationPointer+2
	
	lda #<.actual
	sta c64unit_ActualMemoryLocationPointer+1
	lda #>.actual
	sta c64unit_ActualMemoryLocationPointer+2
		
	ldx #<.length
	ldy #>.length
	jsr c64unit_AssertMemoryEqual
}


; @param word .expected
; @param word .actual
; @param word .length
; @param word .customMessage
; @param word .customMessageEnd
!macro assertMemoryEqual .expected, .actual, .length, .customMessage, .customMessageEnd {
	lda #<.expected
	sta c64unit_ExpectedMemoryLocationPointer+1
	lda #>.expected
	sta c64unit_ExpectedMemoryLocationPointer+2
	
	lda #<.actual
	sta c64unit_ActualMemoryLocationPointer+1
	lda #>.actual
	sta c64unit_ActualMemoryLocationPointer+2
	
	+prepareCustomMessage .customMessage, .customMessageEnd
	
	ldx #<.length
	ldy #>.length
	jsr c64unit_AssertMemoryEqual
}


!macro assertNegativeFlagSignedNotSet {
	jsr c64unit_StoreNotNegativeFlagStatusSigned
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertNegativeFlagSignedNotSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreNotNegativeFlagStatusSigned
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


!macro assertNegativeFlagSignedSet {
	jsr c64unit_StoreNegativeFlagStatusSigned
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertNegativeFlagSignedSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreNegativeFlagStatusSigned
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


!macro assertNegativeFlagUnsignedNotSet {
	jsr c64unit_StoreNotNegativeFlagStatusUnsigned
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertNegativeFlagUnsignedNotSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreNotNegativeFlagStatusUnsigned
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


!macro assertNegativeFlagUnsignedSet {
	jsr c64unit_StoreNegativeFlagStatusUnsigned
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertNegativeFlagUnsignedSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreNegativeFlagStatusUnsigned
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


; @param byte .expected
; @param byte .actual
!macro assertNotEqual .expected, .actual {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	jsr c64unit_AssertNotEqual
}


; @param byte .expected
; @param byte .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertNotEqual .expected, .actual, .customMessage, .customMessageEnd {
	lda #.expected
	sta c64unit_expected
	lda .actual
	sta c64unit_actual
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertNotEqual
}


; @param byte .expected
!macro assertNotEqualToA .expected {
	sta c64unit_actual
	lda #.expected
	sta c64unit_expected
	jsr c64unit_AssertNotEqual
}


; @param byte .expected
; @param word .customMessage
; @param word .customMessageEnd
!macro assertNotEqualToA .expected, .customMessage, .customMessageEnd {
	sta c64unit_actual
	lda #.expected
	sta c64unit_expected
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertNotEqual
}


; @param byte .expected
!macro assertNotEqualToX .expected {
	stx c64unit_actual
	lda #.expected
	sta c64unit_expected
	jsr c64unit_AssertNotEqual
}


; @param byte .expected
; @param word .customMessage
; @param word .customMessageEnd
!macro assertNotEqualToX .expected, .customMessage, .customMessageEnd {
	stx c64unit_actual
	lda #.expected
	sta c64unit_expected
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertNotEqual
}


; @param byte .expected
!macro assertNotEqualToY .expected {
	sty c64unit_actual
	lda #.expected
	sta c64unit_expected
	jsr c64unit_AssertNotEqual
}


; @param byte .expected
; @param word .customMessage
; @param word .customMessageEnd
!macro assertNotEqualToY .expected, .customMessage, .customMessageEnd {
	sty c64unit_actual
	lda #.expected
	sta c64unit_expected
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertNotEqual
}


!macro assertOverflowFlagNotSet {
	jsr c64unit_StoreNotOverflowFlagStatus
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertOverflowFlagNotSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreNotOverflowFlagStatus
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


!macro assertOverflowFlagSet {
	jsr c64unit_StoreOverflowFlagStatus
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertOverflowFlagSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreOverflowFlagStatus
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


; @param word .expected
; @param word .actual
!macro assertWordEqual .expected, .actual {
	ldx #<.expected
	ldy #>.expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	jsr c64unit_AssertWordEqual
}


; @param word .expected
; @param word .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertWordEqual .expected, .actual, .customMessage, .customMessageEnd {
	ldx #<.expected
	ldy #>.expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertWordEqual
}


; @param word .expected
; @param word .actual
!macro assertWordNotEqual .expected, .actual {
	ldx #<.expected
	ldy #>.expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	jsr c64unit_AssertWordNotEqual
}


; @param word .expected
; @param word .actual
; @param word .customMessage
; @param word .customMessageEnd
!macro assertWordNotEqual .expected, .actual, .customMessage, .customMessageEnd {
	ldx #<.expected
	ldy #>.expected
	jsr c64unit_SetExpectedImmediateWord
	
	ldx #<.actual
	ldy #>.actual
	jsr c64unit_SetActualAbsoluteWord
	
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertWordNotEqual
}


!macro assertZeroFlagNotSet {
	jsr c64unit_StoreNotZeroFlagStatus
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertZeroFlagNotSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreNotZeroFlagStatus
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


!macro assertZeroFlagSet {
	jsr c64unit_StoreZeroFlagStatus
	jsr c64unit_AssertFlagSet
}


; @param word .customMessage
; @param word .customMessageEnd
!macro assertZeroFlagSet .customMessage, .customMessageEnd {
	jsr c64unit_StoreZeroFlagStatus
	+prepareCustomMessage .customMessage, .customMessageEnd
	jsr c64unit_AssertFlagSet
}


!macro c64unit {
	+c64unit 1, $0400
}


; @param byte .exitToBasicMode
!macro c64unit .exitToBasicMode {
	+c64unit .exitToBasicMode, $0400
}


; @param byte .exitToBasicMode
; @param word .screenLocation
!macro c64unit .exitToBasicMode, .screenLocation {
*=$0801
	!word .nextBasicLine
	!word 10
	!byte $9e
	!scr "2061"
	!byte 0
.nextBasicLine
	!word 0

	lda #.exitToBasicMode
	jsr c64unit_InitExitToBasicMode
	
	ldx #<.screenLocation
	ldy #>.screenLocation
	jsr c64unit_InitScreenLocation
	
	jsr c64unit_InitScreen
}


!macro c64unitExit {
	jmp c64unit_ExitWithSuccess
}


; @param word .testMethod
!macro examineTest .testMethod {
	jsr c64unit_InitTest
	jsr .testMethod
}


; @param word .dataSet
; @param word .output
!macro loadDataSet .dataSet, .output {
	ldx #<.output
	ldy #>.output
	jsr c64unit_PrepareDataSetOutput
	ldx #<.dataSet
	ldy #>.dataSet
	jsr c64unit_LoadDataSet
}


; @param word .dataSet
!macro loadDataSetToA .dataSet {
	+pushXYOnStack
	ldx #<.dataSet
	ldy #>.dataSet
	jsr c64unit_LoadDataSetToA
	sta c64unit_number
	+pullXYFromStack
	lda c64unit_number
}


; @param word .dataSet
; @param word .output
!macro loadDataSetWord .dataSet, .output {
	ldx #<.output
	ldy #>.output
	jsr c64unit_PrepareDataSetWordOutput
	ldx #<.dataSet
	ldy #>.dataSet
	jsr c64unit_LoadDataSetWord
}


; @param word .dataSet
; @param word .outputLo
; @param word .outputHi
!macro loadDataSetWordToLoHi .dataSet, .outputLo, .outputHi {
	ldx #<.outputLo
	ldy #>.outputLo
	jsr c64unit_PrepareDataSetWordLoOutput
	ldx #<.outputHi
	ldy #>.outputHi
	jsr c64unit_PrepareDataSetWordHiOutput
	ldx #<.dataSet
	ldy #>.dataSet
	jsr c64unit_LoadDataSetWordToLoHi
}


; @return carry flag
!macro isDataSetCompleted {
	jsr c64unit_IsDataSetCompleted
}


; @param word .originalMethod
; @param word .mockMethod
!macro mockMethod .originalMethod, .mockMethod {
	ldx #<.originalMethod
	ldy #>.originalMethod
	jsr c64unit_SetOriginalMethodPointer
	
	ldx #<.mockMethod
	ldy #>.mockMethod
	jsr c64unit_SetMockMethodPointer
	
	jsr c64unit_MockMethod
}


; @param word .customMessage
; @param word .customMessageEnd
!macro prepareCustomMessage .customMessage, .customMessageEnd {
	sec
	lda #<.customMessageEnd
	sbc #<.customMessage
	sta c64unit_numberLo
	lda #>.customMessageEnd
	sbc #>.customMessage
	sta c64unit_numberHi
	
	lda c64unit_numberLo
	ldx #<.customMessage
	ldy #>.customMessage
	jsr c64unit_PrepareCustomMessage
}


; @param byte .length
!macro prepareDataSetLength .length {
	lda #.length
	jsr c64unit_PrepareDataSetLength
}


!macro pushXYOnStack {
	txa
	pha
	tya
	pha
}


!macro pullXYFromStack {
	pla
	tay
	pla
	tax
}


; @param word .originalMethod
!macro unmockMethod .originalMethod {
	ldx #<.originalMethod
	ldy #>.originalMethod
	jsr c64unit_UnmockMethod
}
